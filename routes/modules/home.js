const express = require('express')
const router = express.Router()
const Expense = require('../../models/expense')

router.get('/sort', (req, res) => {
  const userId = req.user._id
  const categoryId = Number(req.query.sort)
  if (categoryId === 6) {
    return res.redirect('/')
  }
  Expense.find({ userId, categoryId })
    .lean()
    .then(expenses => {
      let totalAmount = 0
      expenses.forEach(expense => {
        totalAmount += expense.cost
        expense.date.toISOString().split('T')[0].replace(/-/g, '/')
      })
      res.render('index', { expenses, totalAmount, categoryId })
    })
})

router.get('/', (req, res) => {
  const userId = req.user._id
  Expense.find({ userId })
    .lean()
    .then(expenses => {
      let totalAmount = 0
      expenses.forEach(expense => {
        totalAmount += expense.cost
        expense.date = expense.date.toISOString().split('T')[0].replace(/-/g, '/')
      })
      res.render('index', { expenses, totalAmount })
    })
    .catch(err => console.log(err))
})

module.exports = router
