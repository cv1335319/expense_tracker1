const express = require('express')
const router = express.Router()
const Expense = require('../../models/expense')

// Create
router.get('/new', (req, res) => {
  res.render('new')
})
router.post('/', (req, res) => {
  const { name, date, categoryId, cost } = req.body
  const userId = req.user._id
  const errors =[]
  if (!name || !date|| !categoryId || !cost) {
    errors.push({ message: '欄位皆為必填' })
  }
  if (errors.length) {
    return res.render('new', { errors, name, date, categoryId, cost })
  }
  Expense.create({ ...req.body, userId })
    .then(() => {
      res.redirect('/')
    })
    .catch(err => console.log(err))
})

// Update
router.get('/:id/edit', (req, res) => {
  const id = req.params.id
  const userId = req.user._id
  Expense.findOne({ _id:id, userId })
    .lean()
    .then(expense => {
      expense.date = expense.date.toISOString().split('T')[0]
      res.render('edit', { expense })
    })
    .catch(err => console.log(err))
})
router.put('/:id', (req, res) => {
  const { name, date, categoryId, cost } = req.body
  const id = req.params.id
  const updatedExpense = {...req.body, _id: id}
  const userId = req.user._id
  const errors =[]
  if (!name || !date|| !categoryId || !cost) {
    errors.push({ message: '欄位皆為必填' })
  }
  if (errors.length) {
    return res.render('edit', { errors, expense: updatedExpense })
  }
  Expense.findOneAndUpdate({ _id:id, userId }, updatedExpense )
    .then(() => {
      res.redirect('/')
    })
    .catch(err => console.log(err))
})

// Delete
router.delete('/:id', (req, res) => {
  const id = req.params.id
  const userId = req.user._id
  Expense.findByIdAndDelete({ _id: id, userId })
    .then(() => res.redirect('/'))
    .catch(err => console.log(err))
})

module.exports = router
