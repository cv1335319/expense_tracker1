import globals from "globals";
import pluginJs from "@eslint/js";


export default [
  {files: ["**/*.js"], languageOptions: {sourceType: "commonjs"}},
  {languageOptions: {
    sourceType: 'module',
    globals:{
      ...globals.node,
      ...globals.browser
    }
  }},
  pluginJs.configs.recommended,
];