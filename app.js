const express = require('express')
const app = express()
const port = 3000
const { engine } = require('express-handlebars')
const routes = require('./routes')
const methodOverride = require('method-override')
const hbsHelpers = require('./hbsHelpers.js')
const session = require('express-session')
const usePassport = require('./config/passport')
const flash = require('connect-flash')

require('dotenv').config()
require('./config/mongoose')

app.engine('hbs', engine({ extname: 'hbs', defaultLayout: 'main', helpers: hbsHelpers }))
app.set('view engine', 'hbs')
app.use(express.static('public'))
app.use(express.urlencoded({ extended: true }))
app.use(methodOverride('_method'))
app.use(session({
  secret: 'secret',
  resave: false,
  saveUninitialized: true
}))
app.use(flash())
usePassport(app)
app.use((req, res, next) => {
  res.locals.isAuthenticated = req.isAuthenticated()
  res.locals.user = req.user
  res.locals.success_msg = req.flash('success_msg')
  res.locals.warning_msg = req.flash('warning_msg')
  next()
})


app.use(routes)
app.listen(port, () => {
  console.log(`server is running on http://localhost:${port}`)
})
