const mongoose = require('mongoose')
const Schema = mongoose.Schema
const recordSchema = new Schema({
  name: {name: String, required: true}
})

module.exports = mongoose.model('A3category', recordSchema)
