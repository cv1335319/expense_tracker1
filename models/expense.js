const mongoose = require('mongoose')
const Schema = mongoose.Schema
const expenseSchema = new Schema({
  name: {type: String, required: true},
  date: {type: Date, required: true},
  cost: {type: Number, required: true},
  userId: {
    type: mongoose.Types.ObjectId,
    ref: 'User'
  },
  categoryId: {type: Number, required: true}
})

module.exports = mongoose.model('A3expense', expenseSchema)
